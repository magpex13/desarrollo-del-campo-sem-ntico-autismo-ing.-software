package com.example.hermanos.myapplication;

import android.graphics.Bitmap;

/**
 * Created by Hermanos on 07/11/2015.
 */
public class Fruta {

    private Bitmap bmpFruta;
    private String strNombre;
    private float xFruta;
    private float yFruta;
    private boolean esDestino;


    public Fruta(Bitmap bmpFruta, String strNombre, float xFruta, float yFruta, boolean esDestino) {

        if(esDestino)
            bmpFruta= FiltrosImagen.imagenBinario(bmpFruta);

        this.bmpFruta = bmpFruta;
        this.strNombre = strNombre;
        this.xFruta = xFruta;
        this.yFruta = yFruta;
        this.esDestino=esDestino;

    }

    public Fruta(Bitmap bmpFruta, String strNombre) {
        this.bmpFruta = bmpFruta;
        this.strNombre = strNombre;
        esDestino=false;
    }

    public void setBmpFruta(Bitmap bmpFruta) {

        this.bmpFruta = bmpFruta;
    }

    public void setStrNombre(String strNombre) {
        this.strNombre = strNombre;
    }

    public void setesDestino(boolean esDestino) {
        this.esDestino = esDestino;
    }

    public Bitmap getBmpFruta() {

        return bmpFruta;
    }

    public String getStrNombre() {
        return strNombre;
    }

    public float getyFruta() {
        return yFruta;
    }

    public float getxFruta() {
        return xFruta;
    }

    public boolean isesDestino() {
        return esDestino;
    }
}
