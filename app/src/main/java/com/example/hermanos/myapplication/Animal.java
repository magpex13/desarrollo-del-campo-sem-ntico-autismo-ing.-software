package com.example.hermanos.myapplication;

import android.graphics.Bitmap;

/**
 * Created by Magno on 10/11/2015.
 */
public class Animal {

    private Bitmap bmpAnimal;
    private String strNombre;
    private float x;
    private float y;
    private boolean esDestino;
    private int idAudio;


    public Animal(Bitmap bmpAnimal, String strNombre, float x, float y, boolean esDestino) {
        this.bmpAnimal = bmpAnimal;
        this.strNombre = strNombre;
        this.x = x;
        this.y = y;
        this.esDestino=esDestino;
    }

    public Animal(Bitmap bmpAnimal, String strNombre) {
        this.bmpAnimal = bmpAnimal;
        this.strNombre = strNombre;
        esDestino=false;
    }

    public Animal(Bitmap bmpAnimal, String strNombre,int idAudio) {
        this.bmpAnimal = bmpAnimal;
        this.strNombre = strNombre;
        this.idAudio=idAudio;
        esDestino=false;
    }

    public void setBmpAnimal(Bitmap bmpAnimal) {

        this.bmpAnimal = bmpAnimal;
    }

    public void setStrNombre(String strNombre) {
        this.strNombre = strNombre;
    }

    public void setIdAudio(int idAudio) {
        this.idAudio = idAudio;
    }

    public void setesDestino(boolean esDestino) {
        this.esDestino = esDestino;
    }

    public Bitmap getBmpAnimal() {

        return bmpAnimal;
    }

    public String getStrNombre() {
        return strNombre;
    }

    public float getY() {
        return y;
    }

    public float getX() {
        return x;
    }

    public int getIdAudio() {
        return idAudio;
    }


    public boolean isesDestino() {
        return esDestino;
    }
}
