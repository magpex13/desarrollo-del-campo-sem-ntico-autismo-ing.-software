package com.example.hermanos.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class PersonalizarActivity extends AppCompatActivity {

    private ImageView imgOri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personalizar);

        imgOri = (ImageView) findViewById(R.id.imgOriginal);
        imgOri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Selecciona la imagen"), 1);
            }
        });

        ((Button) findViewById(R.id.btnConvierteBinario)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convierteBinario();
            }
        });

        ((ImageButton)findViewById(R.id.imgBtnAtras)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (resCode == RESULT_OK)
            if (reqCode == 1) {
                imgOri.setImageURI(data.getData());
            }
    }

    public void convierteBinario() {

     ((ImageView) findViewById(R.id.imgBinario)).setImageBitmap(FiltrosImagen.imagenBinario(((BitmapDrawable)imgOri.getDrawable()).getBitmap()));

    }


}
