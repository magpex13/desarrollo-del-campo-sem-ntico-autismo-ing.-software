package com.example.hermanos.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MenuPrincipalActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        ((Button)findViewById(R.id.btnRefuerzo)).setOnClickListener(this);
        ((Button)findViewById(R.id.btnEnsenanza)).setOnClickListener(this);
        ((Button)findViewById(R.id.btnEvaluacion)).setOnClickListener(this);
        ((Button)findViewById(R.id.btnPictogramas)).setOnClickListener(this);
        ((Button)findViewById(R.id.btnPersonalizacion)).setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnRefuerzo : finish();lanzarActivity(RefuerzoActivity.class); break;
            case R.id.btnEnsenanza : finish();lanzarActivity(EnsenanzaActivity.class); break;
            case R.id.btnEvaluacion : finish();lanzarActivity(EvaluacionActivity.class); break;
            case R.id.btnPictogramas : finish();lanzarActivity(PictogramasActivity.class); break;
            case R.id.btnPersonalizacion : finish();lanzarActivity(PersonalizarActivity.class); break;
        }

    }

    void lanzarActivity(Class<?> cls)
    {
        startActivity(new Intent(this,cls));
    }
}
