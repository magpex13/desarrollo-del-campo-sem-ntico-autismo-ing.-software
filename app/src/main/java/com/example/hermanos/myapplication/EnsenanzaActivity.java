package com.example.hermanos.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class EnsenanzaActivity extends AppCompatActivity implements View.OnClickListener {

    private Animal[] vecAnimales;
    private int indice;
    private int capacidadMaximaVectorAnimales;
    private ImageButton imgViewAnimal;
    private TextView txtViewAnimal;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ensenanza);

        ((Button)findViewById(R.id.btnSiguiente)).setOnClickListener(this);
        ((ImageButton)findViewById(R.id.btnAtras)).setOnClickListener(this);
        (imgViewAnimal=(ImageButton)findViewById(R.id.imgBtnAnimal)).setOnClickListener(this);
        txtViewAnimal=(TextView)findViewById(R.id.txtViewAnimal);
        iniciarDatos();
        imgViewAnimal.setImageBitmap(vecAnimales[indice].getBmpAnimal());
        txtViewAnimal.setText(vecAnimales[indice].getStrNombre());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ensenanza, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void iniciarDatos()
    {
        capacidadMaximaVectorAnimales=3;
        vecAnimales= new Animal[capacidadMaximaVectorAnimales];
        vecAnimales[0]= new Animal(BitmapFactory.decodeResource(getResources(),R.drawable.perromax),"Perro",R.raw.perro);
        vecAnimales[1]= new Animal(BitmapFactory.decodeResource(getResources(),R.drawable.vaca),"Vaca",R.raw.vaca);
        vecAnimales[2]= new Animal(BitmapFactory.decodeResource(getResources(),R.drawable.leon),"Leon",R.raw.leon);
        indice=0;
        mediaPlayer=MediaPlayer.create(this,vecAnimales[indice].getIdAudio());

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.imgBtnAnimal: reproducirAudioAnimal(); break;
            case R.id.btnAtras: cerrarActivity(); break;
            case R.id.btnSiguiente: {siguienteAnimal(); break;}

        }

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mediaPlayer.release();
    }

    public void reproducirAudioAnimal()
    {
        mediaPlayer.start();
    }

    public void siguienteAnimal()
    {
        if(indice>=capacidadMaximaVectorAnimales-1)
            return;

        indice++;
        imgViewAnimal.setImageBitmap(vecAnimales[indice].getBmpAnimal());
        txtViewAnimal.setText(vecAnimales[indice].getStrNombre());
        mediaPlayer=MediaPlayer.create(this,vecAnimales[indice].getIdAudio());




    }

    public void cerrarActivity()
    {
        finish();
        lanzarActivity(MenuPrincipalActivity.class);
    }
    void lanzarActivity(Class<?> cls) {startActivity(new Intent(this, cls));}
}
