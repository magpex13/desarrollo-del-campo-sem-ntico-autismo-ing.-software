package com.example.hermanos.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class RefuerzoActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refuerzo);

        ((Button)findViewById(R.id.btnVisual)).setOnClickListener(this);
        ((Button)findViewById(R.id.btnCognitivo)).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnVisual : finish(); lanzarActivity(DesarrolloVisualActivity.class); break;
            case R.id.btnCognitivo : finish(); lanzarActivity(DesarrolloCognitivoActivity.class); break;

        }

    }

    void lanzarActivity(Class<?> cls)
    {
        startActivity(new Intent(this,cls));
    }
    public void cerrarActivity(View view)
    {
        finish();
        lanzarActivity(MenuPrincipalActivity.class);
    }
}
