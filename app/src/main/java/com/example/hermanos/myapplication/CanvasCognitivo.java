package com.example.hermanos.myapplication;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Hermanos on 07/11/2015.
 */
public class CanvasCognitivo extends View {

    private VectorDeLineas vecLineas;
    private VectorDeLineas vecCuadrados;
    private Fruta[] vecFrutas;
    private int posEscogido;
    private float escala;
    private Paint p1;

    public CanvasCognitivo(Context context) {
        super(context);
    }

    public CanvasCognitivo(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CanvasCognitivo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CanvasCognitivo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initDatos()
    {
        Resources resources = getContext().getResources();
        escala=resources.getDisplayMetrics().density;

        vecLineas=new VectorDeLineas();

        vecFrutas= new Fruta[5];

        vecFrutas[0]=new Fruta(BitmapFactory.decodeResource(resources, R.drawable.manzana),"manzana",(int)((float)(this.getWidth()/4)-escala),(int)((float)(this.getHeight()/4)-escala),false);
        vecFrutas[1]=new Fruta(BitmapFactory.decodeResource(resources,R.drawable.manzana2),"manzana",(int)((float)(this.getWidth()/2)-escala),1,true);
        vecFrutas[2]=new Fruta(BitmapFactory.decodeResource(resources,R.drawable.manzana3),"manzana",(int)((float)(this.getWidth()/2)-escala),300,true);
        vecFrutas[3]=new Fruta(BitmapFactory.decodeResource(resources,R.drawable.pina),"piña",(int)((float)(this.getWidth()/4)-escala),(int)((float)(this.getHeight()/2)-escala),false);
        vecFrutas[4]=new Fruta(BitmapFactory.decodeResource(resources,R.drawable.pina2),"piña",(int)((float)(this.getWidth()/2)-escala),150,true);

        posEscogido=-1;

        p1= new Paint();
        p1.setColor(Color.BLACK);

        Paint p2 = new Paint();
        p2.setColor(Color.RED);
        p2.setStyle(Paint.Style.FILL);
        p2.setStrokeWidth(20);
        vecCuadrados=new VectorDeLineas(p2);


    }

    private boolean touchImagen(float xTouch,float yTouch)
    {
        for (int i=0 ; i<vecFrutas.length ; i++)
        {
            if(seleccionarImagen(vecFrutas[i].getxFruta(),vecFrutas[i].getyFruta(),vecFrutas[i].getBmpFruta().getWidth(),vecFrutas[i].getBmpFruta().getHeight(),xTouch,yTouch)) {

                if(posEscogido!=-1)
                {
                    if(lineaValida(i))
                    {
                        //Insertar nueva linea
                        float xStart=vecFrutas[posEscogido].getxFruta()+(vecFrutas[posEscogido].getBmpFruta().getWidth()/2);
                        float yStart=vecFrutas[posEscogido].getyFruta()+(vecFrutas[posEscogido].getBmpFruta().getHeight()/2);
                        float xStop=vecFrutas[i].getxFruta()+(vecFrutas[i].getBmpFruta().getWidth()/2);
                        float yStop=vecFrutas[i].getyFruta()+(vecFrutas[i].getBmpFruta().getHeight()/2);

                        vecLineas.insertarLinea(xStart, yStart, xStop, yStop);
                        vecCuadrados.eliminarTodos();

                    }

                    posEscogido=-1;
                    vecCuadrados.eliminarTodos();
                    return true;
                }
                posEscogido=i;
                vecCuadrados.insertarLineaCuadrado(vecFrutas[posEscogido].getxFruta()-10,vecFrutas[posEscogido].getyFruta()-10,vecFrutas[posEscogido].getBmpFruta().getWidth()+10,vecFrutas[posEscogido].getBmpFruta().getHeight()+10);
                //textView.setText(vecFruta[i].getStrNombre());
                return true;
            }
        }
        return false;
    }

    private boolean lineaValida(int posDestino)
    {
        if(posEscogido==-1)
            return false;

        if(posEscogido==posDestino)
            return false;

        if(!vecFrutas[posEscogido].getStrNombre().equals(vecFrutas[posDestino].getStrNombre()))
            return false;

        if(vecFrutas[posDestino].isesDestino() && vecFrutas[posEscogido].isesDestino())
            return false;

        return true;
    }

    private boolean seleccionarImagen(float xImg,float yImg,int wImg,int hImg,float xTouch,float yTouch)
    {
        if(xTouch<=xImg+ wImg && xTouch>=xImg && yTouch<=yImg+hImg && yTouch>=yImg) {
            return true;
        }
        return false;
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        canvas.drawColor(Color.GRAY);

        for (int i=0 ; i<vecFrutas.length ; i++)
        {
            canvas.drawBitmap(vecFrutas[i].getBmpFruta(),vecFrutas[i].getxFruta(),vecFrutas[i].getyFruta(),null);
        }

        vecLineas.dibujarLineas(canvas);
        vecCuadrados.dibujarLineas(canvas);

        invalidate();
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        initDatos();
    }

    @Override
    public boolean onTouchEvent(MotionEvent e)
    {

        if(e.getAction()==e.ACTION_UP)
            touchImagen(e.getX(), e.getY());


        return true;
    }
}
