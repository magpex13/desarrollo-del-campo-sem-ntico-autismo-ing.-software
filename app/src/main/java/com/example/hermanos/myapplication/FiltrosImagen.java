package com.example.hermanos.myapplication;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

/**
 * Created by Magno on 22/11/2015.
 */
public abstract class FiltrosImagen {

    public static Bitmap imagenBinario(Bitmap imagen)
    {
        try
        {
            if(!imagen.isMutable()) {
                imagen = imagen.copy(imagen.getConfig(), true);
            }

            int pix,gray;
            int umbral = 127;

            for (int i = 0; i < imagen.getWidth(); i++) {
                for (int k = 0; k < imagen.getHeight(); k++) {
                    pix = imagen.getPixel(i, k);
                    gray = (Color.red(pix) + Color.green(pix) + Color.blue(pix)) / 3;

                    if (gray < umbral)
                        gray = 0;
                    else
                        gray = 255;

                    imagen.setPixel(i, k, Color.argb(Color.alpha(pix), gray, gray, gray));
                }
            }

            return imagen;
        }catch (Exception ex)
        {
            return null;
        }
    }
}
