package com.example.hermanos.myapplication;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Hermanos on 07/11/2015.
 */
public class VectorDeLineas {

    private float[] vecLineas;
    private int cantElementos;
    private Paint paintLinea;

    public VectorDeLineas()
    {
        cantElementos=0;
        paintLinea= new Paint();
        paintLinea.setColor(Color.GREEN);
    }

    public VectorDeLineas(int color)
    {
        cantElementos=0;
        paintLinea= new Paint();
        paintLinea.setColor(color);
    }
    public VectorDeLineas(Paint pincel)
    {
        cantElementos=0;
        paintLinea= pincel;
    }

    public boolean insertarLinea(float xStart,float yStart,float xStop,float yStop)
    {
        if(vecLineas==null)
        {
            vecLineas= new float[4];

            vecLineas[0]=xStart;
            vecLineas[1]=yStart;
            vecLineas[2]=xStop;
            vecLineas[3]=yStop;

            cantElementos=4;
            return true;
        }

        float aux[]= new float[cantElementos+4];

        for (int i=0 ; i<vecLineas.length ; i++)
        {
            aux[i]=vecLineas[i];
        }

        aux[cantElementos]=xStart;
        aux[cantElementos+1]=yStart;
        aux[cantElementos+2]=xStop;
        aux[cantElementos+3]=yStop;

        vecLineas=aux;

        cantElementos+=4;

        return true;
    }

    public boolean insertarLineaCuadrado(float xStart,float yStart,float width,float height)
    {
        if(vecLineas==null)
        {
            vecLineas= new float[16];

            vecLineas[0]=xStart;
            vecLineas[1]=yStart;
            vecLineas[2]=xStart+width;
            vecLineas[3]=yStart;

            vecLineas[4]=xStart;
            vecLineas[5]=yStart+height;
            vecLineas[6]=xStart+width;
            vecLineas[7]=yStart+height;

            vecLineas[8]=xStart;
            vecLineas[9]=yStart;
            vecLineas[10]=xStart;
            vecLineas[11]=yStart+height;

            vecLineas[12]=xStart+width;
            vecLineas[13]=yStart;
            vecLineas[14]=xStart+width;
            vecLineas[15]=yStart+height;

            cantElementos=16;
            return true;
        }

        float aux[]= new float[cantElementos+16];

        for (int i=0 ; i<vecLineas.length ; i++)
        {
            aux[i]=vecLineas[i];
        }

        aux[cantElementos]=xStart;
        aux[cantElementos+1]=yStart;
        aux[cantElementos+2]=xStart+width;
        aux[cantElementos+3]=yStart;

        aux[cantElementos+4]=xStart;
        aux[cantElementos+5]=yStart+height;
        aux[cantElementos+6]=xStart+width;
        aux[cantElementos+7]=yStart+height;

        aux[cantElementos+8]=xStart;
        aux[cantElementos+9]=yStart;
        aux[cantElementos+10]=xStart;
        aux[cantElementos+11]=yStart+height;

        aux[cantElementos+12]=xStart+width;
        aux[cantElementos+13]=yStart;
        aux[cantElementos+14]=xStart+width;
        aux[cantElementos+15]=yStart+height;

        vecLineas=aux;

        cantElementos+=16;

        return true;
    }

    public boolean eliminarTodos()
    {
        cantElementos=0;
        vecLineas=null;
        return true;
    }

    public void dibujarLineas(Canvas canvas)
    {
        if(vecLineas!=null)
            canvas.drawLines(vecLineas,paintLinea);
    }

}
